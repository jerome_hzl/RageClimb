// Fill out your copyright notice in the Description page of Project Settings.


#include "HandController.h"

#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"


// Sets default values
AHandController::AHandController()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MotionController"));
	SetRootComponent(MotionController);

	// SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshHand"));
	// SkeletalMesh->SetupAttachment(MotionController);
}

// Called when the game starts or when spawned
void AHandController::BeginPlay()
{
	Super::BeginPlay();

	OnActorBeginOverlap.AddDynamic(this, &AHandController::ActorBeginOverlap);
	OnActorEndOverlap.AddDynamic(this, &AHandController::ActorEndOverlap);
}

// Called every frame
void AHandController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SkeletalMeshHand->SetLocationAndRotationOfRootScene(MotionController->GetComponentLocation(), MotionController->GetComponentRotation(),
	                                                    bIsClimbing);
	APawn* Pawn = Cast<APawn>(GetAttachParentActor());
	if (bIsClimbing && Pawn)
	{
		SkeletalMeshHand->Climbing(Pawn, GetActorLocation());
		// Muss an dieser STelle gesetzt werden. in CLimbing() funktioniert es nicht
		SkeletalMeshHand->ClimbingStartLocation = GetActorLocation();
	}
}

// Kann nicht in BeginPlay() gespawnt werden, da die TrackingSource erst später gesetzt wird
void AHandController::SpawnSkeletalMeshHand()
{
	// Überprüfe ob es der Linke oder Rechte Controller ist und Spwane die entsprechende Hand.
	if (MotionController->GetTrackingSource() == EControllerHand::Right)
	{
		SkeletalMeshHand = GetWorld()->SpawnActor<ASkeletalMeshHand>(SkeletalMeshClassRight, GetActorLocation(), GetActorRotation());
		SkeletalMeshHand->Rename(TEXT("SkeletalMeshHandRight"));
	}
	else
	{
		SkeletalMeshHand = GetWorld()->SpawnActor<ASkeletalMeshHand>(SkeletalMeshClassLeft, GetActorLocation(), GetActorRotation());
		SkeletalMeshHand->Rename(TEXT("SkeletalMeshHandLeft"));
	}

	if (SkeletalMeshHand)
	{
		SkeletalMeshHandComponent = SkeletalMeshHand->GetSkeletalMeshComponent();
		// if (SkeletalMeshHandComponent) SetHandAnimInstanceInBlueprint();
		GunSocket = SkeletalMeshHandComponent->GetSocketByName(TEXT("hand_gunSocket"));
	}
}

void AHandController::PairController(AHandController* Controller)
{
	if (!Controller) return;

	OtherController = Controller;
	OtherController->OtherController = this;
}

void AHandController::Grab()
{
	if (!bCanClimb)
	{
		GrabGun();
		return;
	}

	// ClimbingStartLocation soll nur einmal zu Beginn gesetzt werden
	if (!bIsClimbing)
	{
		SkeletalMeshHand->ClimbingStartLocation = GetActorLocation();
		bIsClimbing = true;
		SkeletalMeshHand->StartHandAnim(EHandAnim::Grab);

		// Used for only climb with one hand at the same time
		// OtherController->bIsClimbing = false;
	}
}

void AHandController::Release()
{
	if (!bCanClimb && GrippedGun)
	{
		ReleaseGun();
		return;
	}
	
	if (bIsClimbing)
	{
		bIsClimbing = false;
		SkeletalMeshHand->StartHandAnim(EHandAnim::Open);
	}
}

bool AHandController::CanClimb() const
{
	TArray<AActor*> OverlapingActors;
	GetOverlappingActors(OUT OverlapingActors);

	for (AActor* Actor : OverlapingActors)
	{
		if (Actor->ActorHasTag(TEXT("Climbable"))) return true;
	}

	return false;
}

void AHandController::ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	bool bNewCanClimb = CanClimb();

	// Haptic effect?

	bCanClimb = bNewCanClimb;

	// For performance and for short no overlaping while climbing
	if (!bIsClimbing && !bIsHoldingGun && bCanClimb) SkeletalMeshHand->StartHandAnim(EHandAnim::CanGrab);
}

void AHandController::ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	bCanClimb = CanClimb();
	if (!bIsClimbing && !bIsHoldingGun) SkeletalMeshHand->StartHandAnim(EHandAnim::Open);
}

void AHandController::GrabGun()
{
	if (MotionController->GetTrackingSource() != EControllerHand::Right) return;
	
	TArray<AActor*> OverlapingActors;
	GetOverlappingActors(OUT OverlapingActors);
	
	for (AActor* Actor : OverlapingActors)
	{
		if (Actor->ActorHasTag(TEXT("GrappleGun")))
		{
			bIsHoldingGun = true;
			SkeletalMeshHand->StartHandAnim(EHandAnim::GunGrab);
			Actor->AttachToComponent(SkeletalMeshHandComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, GunSocket->SocketName);
			GrippedGun = Actor;
		}
	}
}

void AHandController::ReleaseGun()
{
	if (!GrippedGun) return;
	
	GrippedGun->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	SkeletalMeshHand->StartHandAnim(EHandAnim::Open);
	GrippedGun = nullptr;
}
