// Fill out your copyright notice in the Description page of Project Settings.


#include "VRCharacter.h"
#include "HandController.h"

// Sets default values
AVRCharacter::AVRCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VRRoot = CreateDefaultSubobject<USceneComponent>("VRRoot");
	VRRoot->SetupAttachment(GetRootComponent());
	
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(VRRoot);
	
}

// Called when the game starts or when spawned
void AVRCharacter::BeginPlay()
{
	Super::BeginPlay();

	SetupController();
	
}

// Called every frame
void AVRCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Charackter und Kamera sind nicht an der gleichen Stelle, wenn man sich bewegt. Haben also einen Offset. Wird hierdruch behoben
	FVector NewCameraOffset = Camera->GetComponentLocation() - GetActorLocation();
	NewCameraOffset.Z = 0;
	AddActorWorldOffset(NewCameraOffset);
	VRRoot->AddWorldOffset(-NewCameraOffset);

}

// Called to bind functionality to input
void AVRCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("GribLeft"), IE_Pressed, this, &AVRCharacter::GrabLeft);
	PlayerInputComponent->BindAction(TEXT("GribLeft"), IE_Released, this, &AVRCharacter::ReleaseLeft);

	PlayerInputComponent->BindAction(TEXT("GribRight"), IE_Pressed, this, &AVRCharacter::GrabRight);
	PlayerInputComponent->BindAction(TEXT("GribRight"), IE_Released, this, &AVRCharacter::ReleaseRight);
	
	PlayerInputComponent->BindAxis(TEXT("Forward"), this, &AVRCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("Right"), this, &AVRCharacter::MoveRight);

}

void AVRCharacter::SetupController()
{
	LeftController = GetWorld()->SpawnActor<AHandController>(HandControllerClass);
	if (LeftController)
	{
		LeftController->SetHand(EControllerHand::Left);
		LeftController->SpawnSkeletalMeshHand();
		LeftController->AttachToComponent(VRRoot,FAttachmentTransformRules::KeepRelativeTransform);
		LeftController->SetOwner(this);
		LeftController->Rename(TEXT("LeftMotionController"));
	}

	RightController = GetWorld()->SpawnActor<AHandController>(HandControllerClass);
	if (RightController)
	{
		RightController->SetHand(EControllerHand::Right);
		RightController->SpawnSkeletalMeshHand();
		RightController->AttachToComponent(VRRoot,FAttachmentTransformRules::KeepRelativeTransform);
		RightController->SetOwner(this);
		RightController->Rename(TEXT("RightMotionController"));
	}

	if (LeftController && RightController)
	{
		// Damit beide Controller von einander wissen
		// LeftController->PairController(RightController);
		
		// LeftControllerAnimInstance = LeftController->GetAnimInstanceFromSkeletalMesh();
		// RightControllerAnimInstance = RightController->GetAnimInstanceFromSkeletalMesh();
	}
	
}

void AVRCharacter::MoveForward(float throttle)
{
	AddMovementInput(throttle * Camera->GetForwardVector());
}

void AVRCharacter::MoveRight(float throttle)
{
	AddMovementInput(throttle * Camera->GetRightVector());
}
