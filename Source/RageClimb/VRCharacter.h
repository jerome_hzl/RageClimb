// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Camera/CameraComponent.h"
#include "HandController.h"
#include "Animation/AnimInstance.h"

#include "GameFramework/Character.h"
#include "VRCharacter.generated.h"

UCLASS()
class RAGECLIMB_API AVRCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AVRCharacter();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	void SetupController();

	void GrabLeft() { LeftController->Grab(); }
	void ReleaseLeft() { LeftController->Release(); }
	void GrabRight() { RightController->Grab(); }
	void ReleaseRight() { RightController->Release(); }

	void MoveForward(float throttle);
	void MoveRight(float throttle);
	
private:

	UPROPERTY(VisibleAnywhere)
	class USceneComponent* VRRoot = nullptr;

	UPROPERTY(EditAnywhere)
	class UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere)
	class AHandController* LeftController;
	
	UPROPERTY(VisibleAnywhere)
	class AHandController* RightController;

	UPROPERTY(VisibleAnywhere)
	UAnimInstance* LeftControllerAnimInstance;

	UPROPERTY(VisibleAnywhere)
	UAnimInstance* RightControllerAnimInstance;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AHandController> HandControllerClass;
};
