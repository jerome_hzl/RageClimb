// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HandAnimEnum.h"
#include "GameFramework/Actor.h"
#include "MotionControllerComponent.h"
#include "SkeletalMeshHand.h"
#include "HandController.generated.h"

UCLASS()
class RAGECLIMB_API AHandController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHandController();

	void SetHand(EControllerHand Hand) {MotionController->SetTrackingSource(Hand);}
	void PairController(AHandController* Controller);
	void SetTransformForHand(FTransform NewHandTransform) { SkeletalMeshHand->SetTransformForLeftHand(NewHandTransform); }

	// Gibt die Animationsinstanz an den Character zurück, für die Controller unterscheidung 
	UAnimInstance* GetAnimInstanceFromSkeletalMesh() { return SkeletalMeshHandComponent->GetAnimInstance(); }

	// Functions
	void SpawnSkeletalMeshHand();
	void Grab();
	void Release();


private:

	bool CanClimb() const;
	void GrabGun();
	void ReleaseGun();
	
	UFUNCTION()
	void ActorBeginOverlap(AActor* OverlappedActor, AActor* OtherActor );
	UFUNCTION()
	void ActorEndOverlap(AActor* OverlappedActor, AActor* OtherActor );
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	// Default sub object
	UPROPERTY(VisibleAnywhere)
	UMotionControllerComponent* MotionController;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="ClimbingVariables")
	ASkeletalMeshHand* SkeletalMeshHand;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="ClimbingVariables")
	USkeletalMeshComponent* SkeletalMeshHandComponent;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess))
	TSubclassOf<ASkeletalMeshHand> SkeletalMeshClassRight;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess))
	TSubclassOf<ASkeletalMeshHand> SkeletalMeshClassLeft;

	UPROPERTY()
	AActor* GrippedGun = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="ClimbingVariables")
	USkeletalMeshSocket const* GunSocket;

	// Parameters
	// UPROPERTY(EditDefaultsOnly)
	// class UHapticFeedbackEffect_Base* HapticEffect;
	//
	
	// State
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="ClimbingVariables")
	bool bCanClimb = false;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="ClimbingVariables")
	bool bIsClimbing = false;

	UPROPERTY(VisibleAnywhere)
	bool bIsHoldingGun = false;
	
	
	FVector ClimbingStartLocation = FVector::Zero();

	AHandController* OtherController;
};
