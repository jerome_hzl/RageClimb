// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RageClimbGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RAGECLIMB_API ARageClimbGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
