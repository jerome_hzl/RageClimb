// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HandAnimEnum.h"
#include "GameFramework/Actor.h"
#include "SkeletalMeshHand.generated.h"

UCLASS()
class RAGECLIMB_API ASkeletalMeshHand : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASkeletalMeshHand();

	void Climbing(APawn* VRCharacter, FVector LocationMotionController);
	void SetTransformForLeftHand(FTransform LeftHandTransform) { SetActorTransform(LeftHandTransform); }
	
	UFUNCTION(BlueprintCallable)
	USkeletalMeshComponent* GetSkeletalMeshComponent() { return SkeletalMeshHand; }

	UFUNCTION(BlueprintImplementableEvent)
	void StartHandAnim(EHandAnim HandAnim);
	
	void SetLocationAndRotationOfRootScene(FVector Location, FRotator Rotation, bool bIsClimbing);

	// Propertys
	UPROPERTY()
	FVector ClimbingStartLocation;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	UPROPERTY(VisibleAnywhere)
	USceneComponent* RootSceneComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = true), Category="SkeletalHandVariables")
	USkeletalMeshComponent* SkeletalMeshHand;
	
};
