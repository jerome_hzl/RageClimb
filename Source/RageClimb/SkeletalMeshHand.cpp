// Fill out your copyright notice in the Description page of Project Settings.


#include "SkeletalMeshHand.h"

// Sets default values
ASkeletalMeshHand::ASkeletalMeshHand()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
	SetRootComponent(RootSceneComponent);
	
	SkeletalMeshHand = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMeshHand"));
	SkeletalMeshHand->SetupAttachment(RootSceneComponent);
	
}

// Called when the game starts or when spawned
void ASkeletalMeshHand::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASkeletalMeshHand::SetLocationAndRotationOfRootScene(FVector Location, FRotator Rotation, bool bIsClimbing)
{
	if (!bIsClimbing) RootSceneComponent->SetWorldLocation(Location);
	else UE_LOG(LogTemp, Warning, TEXT("%s: bIsClimbing: %d"), *GetName(), bIsClimbing); // Wird nur für eine Hand gesetzt? JA!
	
	RootSceneComponent->SetWorldRotation(Rotation);
}

void ASkeletalMeshHand::Climbing(APawn* VRCharacter, FVector LocationMotionController)
{
	
	FVector HandControllerDelta = LocationMotionController - ClimbingStartLocation;
	
	VRCharacter->SetActorLocation(VRCharacter->GetActorLocation() - HandControllerDelta);
	
}
